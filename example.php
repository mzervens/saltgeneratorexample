<?php

function generateSalt()
{
	$characterPool = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890-=!@#$%^&*()_+';
	
	$maxIndex = 75;
	
	$saltString = "";
	
	for($i = 0; $i < 20; $i++)
	{
		$index = rand(0, $maxIndex);
		
		$char = substr($characterPool, $index, 1);
		$saltString .= $char;
		
	}
	
	return $saltString;
	
}